#vpc variables
variable "cidr_vpc1" {
  description = "specify vpc cidr value"
  type        = string
  default     = "10.0.0.0/16"

}

variable "vpc_tag1" {
  description = "specifying vpc name"
  type        = string
  default     = "jenkins-vpc"

}


# subnet variables
variable "pub_cidr_block1" {
  description = "subnet cidr"
  type        = string
  default     = "10.0.1.0/24"
}

variable "pub_subnet_name1" {
  description = "subnet names"
  type        = string
  default     = "jenkins-subnet"

}

variable "availability_zone2" {
  description = "aavailibility zone for subnet"
  type        = string
  default     = "ap-south-1a"

}

#igw variables
variable "igw_name1" {
  description = "igw name"
  type        = string
  default     = "jenkins-igw"
}

#route-table variables
variable "pub_rt_name1" {
  description = "public route table name"
  type        = string
  default     = "jenkins-route-table"
}

#security group variable
variable "sg_name1" {
  description = "security group name"
  type        = string
  default     = "jenkins-security-group"
}

variable "web_ingress1" {
  type = map(object({
    port        = number
    protocol    = string
    cidr_blocks = list(string)
  }))
  default = {
    "port1" = {
      cidr_blocks = ["0.0.0.0/0"]
      port        = 22
      protocol    = "tcp"
    }
    "port2" = {
      cidr_blocks = ["0.0.0.0/0"]
      port        = 8080
      protocol    = "tcp"
    }
  }
}


# Instance variables
variable "ami1" {
  description = "instance ami"
  type        = string
  default     = "ami-02eb7a4783e7e9317"
}

variable "instance_type1" {
  description = "instance type"
  type        = string
  default     = "t2.micro"
}

variable "key_name1" {
  description = "instance key name"
  type        = string
  default     = "anu-server-key"
}


variable "pub_instance_name1" {
  description = "public instance name"
  type        = string
  default     = "jenkins-tool"
}

variable "availability_zone3" {
  description = "availability zone for instance"
  type        = string
  default     = "ap-south-1a"
}
