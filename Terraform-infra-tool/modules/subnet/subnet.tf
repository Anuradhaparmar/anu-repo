resource "aws_subnet" "main1" {
  vpc_id     = var.vpc_id
  cidr_block = var.pub_cidr_block
  availability_zone = var.availability_zone

  tags = {
  
    Name = var.pub_subnet_name
}
}
