resource "aws_security_group" "sg1" {
  description = "Allow inbound traffic"
  vpc_id      = var.vpc_id

dynamic "ingress" {
   for_each = var.web_ingress
   content {
     description = "inbound rules"
     from_port = ingress.value.port
     to_port = ingress.value.port
     protocol = ingress.value.protocol
     cidr_blocks = ingress.value.cidr_blocks
   }
}


  tags = {
    Name = var.sg_name
  }
}
