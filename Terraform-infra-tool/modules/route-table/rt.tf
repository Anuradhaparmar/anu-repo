resource "aws_route_table" "route1" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.gateway_id
  }

  tags = {
    Name = var.pub_rt_name
  }
}

# public subnet association
resource "aws_route_table_association" "ass1" {
  subnet_id      = var.subnet_id
  route_table_id = aws_route_table.route1.id
}
