variable "vpc_cidr" {
  description = "cidr value for vpc"
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_tag" {
  description = "tag value for vpc"
  type        = string
  default     = "jenkins-vpc"
}
